DROP DATABASE IF EXISTS ni950971_1sql2;
CREATE DATABASE ni950971_1sql2;
USE ni950971_1sql2;


CREATE TABLE User(
    id              INTEGER(11) NOT NULL AUTO_INCREMENT,
    firstname       VARCHAR(255),
    surname         VARCHAR(255),
    username        VARCHAR(255),
    password        VARCHAR(32),
    last_login      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);


CREATE TABLE Models(
    serialno        INTEGER(11) NOT NULL AUTO_INCREMENT,
    model           VARCHAR(255),
    kmh             INTEGER(3),
    watt            INTEGER(10),
    amper           INTEGER(10),
    voltage         INTEGER(10),
    type            VARCHAR(255),
    PRIMARY KEY (serialno)
);

CREATE TABLE Trips(
    id              INTEGER(11) NOT NULL AUTO_INCREMENT,
    userid          INTEGER(11),
    modelserialno   INTEGER(11),
    drivingdistance INTEGER(11),
    drivingtime     INTEGER(11),
    PRIMARY KEY (id)
);

/******************************************************************************/
/***                              Foreign Keys                              ***/
/******************************************************************************/
ALTER TABLE Trips ADD FOREIGN KEY (userid) REFERENCES User (id);
ALTER TABLE Trips ADD FOREIGN KEY (modelserialno) REFERENCES Models (serialno);
