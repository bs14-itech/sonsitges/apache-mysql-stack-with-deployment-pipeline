USE ni950971_1sql2;

/* ***************************************************************************************************************** */
/*                                                   Tabellen                                                        */
/* ***************************************************************************************************************** */

/* User */
INSERT INTO User (firstname, surname, username, password, last_login) VALUES ("Max", "Mustermann", "MustermannM", "41933e60e9c19b866b3d68864727afe7", CURRENT_TIMESTAMP);

/* Models */
INSERT INTO Models (model, kmh, watt, amper, voltage, type) VALUES ("Bird X43", 30, 1500, 32, 72, "Street");
INSERT INTO Models (model, kmh, watt, amper, voltage, type) VALUES ("Bird X43 Slim", 23, 1150, 32, 55, "Street");
INSERT INTO Models (model, kmh, watt, amper, voltage, type) VALUES ("Titan 21 Offroader", 35, 1700, 40, 84, "Offroad");
INSERT INTO Models (model, kmh, watt, amper, voltage, type) VALUES ("Aeris M7 Allrounder", 20, 1000, 50, 48, "Long-Distance");
