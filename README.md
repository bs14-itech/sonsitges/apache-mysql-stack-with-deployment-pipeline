# E-Scooter Website

# Docker

Kann unter Linux ausgeführt werden.
Installation von Docker auf Ubuntu:

https://docs.docker.com/engine/install/ubuntu/

Installation von Docker-Compose:

https://docs.docker.com/compose/install/


# Vorbereitung

Starten tut man die Container mit:
```
sudo docker-compose up
```
Die Scripte müssen noch ausführbar gemacht werden:
```
sudo chmod +x renewDatabase.sh

sudo chmod +x database/execSqlFiles.sh
```
Nun kann man mittles des Scripts die beiden Sql-Datein (Dump-Database + testdata) ausführen:
```
sudo ./renewDatabase
```

# Website Login

Link zur [Website](http://localhost:8000)

Auf der Website kann man sich mit folgendem Benutzer anmelden:

Username: MustermannM

Passwort: 123456

# MySQL Datenbank

Den Datenbank-Container kann man erreichen mit 
```
sudo docker exec -it logindb bash
```

Um in die Datenbank zu kommen muss man noch den Befehl
```
mysql -u root -p5d41402abc4b2a76b9719d911017c592
```
ausführen


# Pipeline

Ich habe die Pipeline mit diesen [Video](https://www.youtube.com/watch?v=pChVWAVY7RU) erstellt.

**Gitlab Deploy Token**

Um ein Gitlab Deploy Token zu erstellen gehe zu 

`Settings -> Repository -> Deploy Token`

Hier soll ein Token mit den Namen z.B. "gitlab-deploy-token" erstellt werden

Hier nur auswählen:
- [ ] read_repository
- [ ] read_registry

**Gitlab CI/CD Variables**

um Variablen anzulegen gehe in 

`Settings -> CI/CD -> Variables`

Hier müssen folgende Variablen angegeben werden:
- [ ] MASTER_HOST
    - Dies soll die IP-Addresse des Deployment-Servers eingetragen werden
- [ ] MASTER_SSH_KEY
    - Dies soll den authorisierten SSH-Schlüssel des Deployment-Servers enthalten ([Doku](https://wiki.archlinux.de/title/SSH-Authentifizierung_mit_Schl%C3%BCsselpaaren))
    - außerdem muss dies als File abgespeichert werden
    - Am Ende muss eine "New Line" hinzugefügt werden, da sonst der Schlüssel nicht erkannt wird
- [ ] MASTER_SSH_USER
    - Soll den Benutzername beinhalten


