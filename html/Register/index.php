<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/conn.php');//Datenbankverbindung
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');//Main Settings

//Prüfen ob der Benutzer angemeldet ist
if (strlen($_SESSION['id']!=0)) {
		//Wenn erfolgreich dann, weiterleitung an Main-Page
		header('location:/');
	}
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<!-- Zeichen Koodierung -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$servername?></title>
	<!-- Website Title Logo -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $fav;?>">
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
	<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
</head>
<body>
	<div class="header">
		<nav class="navigation">
			<a class="logo" href="/" target="_self"><?php echo $serverlogo;?></a>
			<ul class="nav">
				<li class="nav-item"><a href="<?php echo $navlink1;?>" class="nav-link"><?php echo $navitem1;?></a></li>
				<li class="nav-item"><a href="<?php echo $navlink2;?>" class="nav-link"><?php echo $navitem2;?></a></li>
				<li class="nav-item"><a href="<?php echo $navlink3;?>" class="nav-link"><?php echo $navitem3;?></a></li>
			</ul>
		</nav>
		<?php
		//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
		if (strlen($_SESSION['id']==0)) {
			//Wenn Benutzer nicht angemeldet
			session_destroy(); // Löscht alle Sessionvariablen
			echo"<nav class='account'>
					<a class='nav-link' href='/Login' target='_self'>Login</a>
					<aside id='account_placeholder'>/</aside>
					<a class='nav-link-active' href='/Register' target='_self'>Register</a>
				</nav>";
		} else {
			echo"<nav class='account'>
					<a class='nav-link' href='/Login' target='_self'>" . $_SESSION['login'] . "</a>
				</nav>";
		}?>
	</div>
		<?php
			//Code für Account Richtlienen
			//Prüfen ob der Knopf gedrückt wurde
			if(isset($_POST['adduser'])) {
				//Speichern der Benutzer eingaben in Variablen
				$vorname = $_POST['vorname'];
				$nachname = $_POST['nachname'];
				$benutzername = $_POST['benutzername'];
				$passwort = $_POST['passwort'];
				//Prüfen ob die Account Richtlienen erfüllt werden
				if(strlen($vorname) < 3) {
					$_SESSION['errmaxlenght']="Firstname too short";
				} elseif (strlen($vorname) > 15) {
					$_SESSION['errmaxlenght']="Firstname too long";
				} elseif (strlen($nachname) < 3) {
					$_SESSION['errmaxlenght']="Lastname too short";
				} elseif (strlen($nachname) > 15) {
					$_SESSION['errmaxlenght']="Lastname too long";
				} elseif (strlen($benutzername) < 3) {
					$_SESSION['errmaxlenght']="Username too short";
				} elseif (strlen($benutzername) > 20) {
					$_SESSION['errmaxlenght']="Username too long";
				} elseif (strlen($passwort) < 6) {
					$_SESSION['errmaxlenght']="Password too short";
				} elseif (strlen($passwort) > 25) {
					$_SESSION['errmaxlenght']="Password too long";
				//Wenn Richtlienen erfolgreich, dann
				} else {
					//verschlüsseln wir das Passwort in md5
					$hashed_passwort = md5($passwort);
					//Benutzer der Datenbank hinzugefügen
					$sql = mysqli_query($con,"INSERT INTO User (`vorname`, `nachname`, `benutzername`, `passwort`)
					VALUES ('$vorname', '$nachname', '$benutzername', '$hashed_passwort')");
					//Setzt die Sessionvariable "addsuccessmsg" auf eine Erfolgsnachricht
					$_SESSION['addsuccessmsg']="Benutzer erfolgreich hinzugefügt";
				}
			}
			if(isset($_SESSION['addsuccessmsg'])) {
				//Wenn erfolgreich dann, erstellen wir ein HTML-Element (p) und geben den Wert Sessionvariable "addsuccessmsg" aus.
				echo "<p id='success'>" . $_SESSION['addsuccessmsg'] . "</p>";
				//Setzt die Sessionvariable "addsuccessmsg" auf den Wert NULL(nichts)
				$_SESSION['addsuccessmsg']=NULL;
			}
		?>
		<form method="post" action="" id="form"> <!-- Erstellen eines Webfomulars mit der Methode "POST" -->
			<h1>Create an account</h1>
			<!-- Erstellen der Eingabefelder -->
			<input type="text" id="vorname" name="vorname" placeholder="First name">
			<input type="text" id="nachname" name="nachname" placeholder="Last name">
			<input type="text" id="username" name="benutzername" placeholder="Username">
			<input type="password" id="password" name="passwort" placeholder="Password">
			<input type="submit" name="adduser" value="Submit">
			<?php  
			if (isset($_SESSION['errmaxlenght'])) {
				//Wenn erfolgreich dann, erstellen wir ein HTML-Element (p) und geben den Wert Sessionvariable "errmaxlenght" aus.
				echo "<p id='errormsg'>" . $_SESSION['errmaxlenght'] . "</p>";
				//Setzt die Sessionvariable auf den Wert NULL(löcht sie wieder)
				$_SESSION['errmaxlenght']=NULL;
			}
			?>
			<!-- Account vorrausetzungen -->
			<aside class="note" id="faside">First name between 3-15 characters</aside>
			<aside class="note">Last name between 3-15 characters</aside>
			<aside class="note">Username between 3-20 characters</aside>
			<aside class="note">Password between 6-25 characters</aside>
		</form>
	<div id="footer">
	  <p>
		<span><?=$servername?> © <?=date("Y")?></span>
		<a class="social" href="<?=$privacypolicylink?>" target="_blank" ><?=$privacypolicy?></a>
		<a class="social" href="<?=$moodlelink?>" target="_blank" ><?=$moodle?></a>
	  </p>
	</div>
	</body>
</html>
