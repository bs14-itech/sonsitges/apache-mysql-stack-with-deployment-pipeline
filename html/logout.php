<?php
session_start();   // Ruft alle Sessionvariablen,
session_destroy(); // löscht diese dann.
session_start();   // Danach erstellen wir eine neue Session Sitzung
//Setzt die Sessionvariable "logoutmsg" auf den Wert "Erfolgreich Abgemeldet"
$_SESSION['logoutmsg']="Erfolgreich abgemeldet"; 
header('location:index.php'); //Weiterleitung an die Startseite
?>
