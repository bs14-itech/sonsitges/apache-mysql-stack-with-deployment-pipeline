<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/conn.php');//Datenbankverbindung
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');//Main Settings

//Prüft ob die Sessionvariable "logoutmsg" gesetzt
if (isset($_SESSION['logoutmsg'])) {
	//Wenn ja, dann erstellen wir ein HTML-Element (p) und geben den Wert Sessionvariable aus.
	echo "<p id='abmelden'>" . $_SESSION['logoutmsg'] . "</p>";
	//Setzt die Sessionvariable auf den Wert NULL(nichts)
	$_SESSION['logoutmsg']=NULL;
}

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<!-- Zeichen Koodierung -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$servername?></title>
	<!-- Website Title Logo -->
	<link rel="shortcut icon" type="image/x-icon" href="<?=$fav?>">
	<!-- Schriftarten -->
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
	<!-- CSS Datein -->
	<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
</head>
<body>
	<section id="box1">
		<!-- Navigation -->
		<div class="header">
			<nav class="navigation">
				<a class="logo" href="/" target="_self"><?=$serverlogo?></a>
				<ul class="nav">
					<li class="nav-item"><a href="<?=$navlink1?>" class="nav-link"><?=$navitem1?></a></li>
					<li class="nav-item"><a href="<?=$navlink2?>" class="nav-link"><?=$navitem2?></a></li>
					<li class="nav-item"><a href="<?=$navlink3?>" class="nav-link"><?=$navitem3?></a></li>
				</ul>
			</nav>
			<?php
			//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
			if (strlen($_SESSION['id']==0)) {
				//Wenn Benutzer nicht angemeldet
				session_destroy(); // Löscht alle Sessionvariablen
				echo"<nav class='account'>
						<a class='nav-link' href='/Login' target='_self'>Login</a>
						<aside id='account_placeholder'>/</aside>
						<a class='nav-link' href='/Register' target='_self'>Register</a>
					</nav>";
			} else {
				echo"<nav class='account'>
						<a id='accountpopup'>" . $_SESSION['login'] . "&#11206;</a>
						<a id='accountpopup' href='/logout.php' target='_self'>Logout</a>
					</nav>";
			}?>
		</div>
		<!-- Main Site -->
		<h1>Scooting with no limits</h1>
		<div id="box2">
			<div id="box3">
				<h4 class="headline">Scooting with no limits</h4>
				<p class="Info">Luckily we have a suitable scooter for any terrain, for example, we have the long-distance scooters, which have a particularly long range. However, these are not the only scooters we can offer you, because we also have an off-road scooter that can drive through any landscape with its extra powerful performance. In addition, our road scooters are perfect for the way to work.</p>
			</div>
			<div id="box4">
				<h4 class="headline">Sustainability</h4>
				<p class="Info">Our scooters are durable and have an average life of 1.5 years. What makes e-scooters special are the mostly short trips without air pollution. In addition, our scooters have low recharge costs. The special thing is that our scooters are distributed all over Los Angeles.</p>
			</div>
		</div>
	</section>
	<div id="footer">
	  <p>
		<span><?=$servername?> © <?=date("Y")?></span>
		<a class="social" href="<?=$privacypolicylink?>" target="_blank" ><?=$privacypolicy?></a>
		<a class="social" href="<?=$moodlelink?>" target="_blank" ><?=$moodle?></a>
	  </p>
	</div>
	</body>
</html>
