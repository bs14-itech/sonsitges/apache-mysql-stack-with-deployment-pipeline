function main() {
	//User input
	let route = document.getElementById("traveledroute").value;
	let model = document.getElementById("models").value;
	//Wir erzeugen eine XMLHttpRequest, mit dem wir Daten über das HTTP-Protokol übertragen können.
	$.ajax({
		url: 'calculate.php',
		type: 'POST',
		data: {
			//Das geht an PHP (zum Server)
			traveledroute: route,
			serialno: model,
		},
		complete: function(data) {
			//Wurde die Seite erfolgreich geladen(HTTP Status code 200)
			if (data.status===200) {
				//Convertieren von String zum Array
				let json = JSON.parse(data.responseText);
				//Entweder True oder False
				if (json.error == true) {
					$('#errormsg').text(json.errormsg);
				} else {
					//Die Werte von PHP einfügen
					document.getElementById("kmh").value = json.kmh;//Schreibt die Variable von PHP in das Textfeld
					document.getElementById("tripprice").value = json.tripprice+json.currency;//Schreibt die Variable von PHP in das Textfeld
					document.getElementById("runtime").value = json.runtime+"min";//Schreibt die Variable von PHP in das Textfeld
					document.getElementById("power").value = json.power+"W";//Schreibt die Variable von PHP in das Textfeld
					document.getElementById("chargecosts").value = json.chargecosts+json.currency;//Schreibt die Variable von PHP in das Textfeld
				}
			} else {
				//'calculate.php' nicht erreichbar
				alert("Fehler bei der Abfrage");
			}
		}
	});
}