<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');
include($_SERVER["DOCUMENT_ROOT"].'/Settings/getScooter.php'); //Importieren der Kosten (Bereits berechnet)
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<!-- Zeichen Koodierung -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$servername?></title>
		<!-- Website Title Logo -->
		<link rel="shortcut icon" type="image/x-icon" href="<?=$fav?>">
		<!-- Schriftarten -->
		<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
		<!-- CSS Datein -->
		<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
		<!-- Importieren der JavaScript / JQUERY - Datei -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<!-- Importieren der JavaScript - Datei -->
		<script type="text/javascript" charset="UTF-8" src="js/dynamic.js"></script>
	</head>
	<body>
		<div class="header">
			<!-- Navigation -->
			<nav class="navigation">
				<a class="logo" href="/" target="_self"><?=$serverlogo?></a>
				<ul class="nav">
					<li class="nav-item"><a href="<?=$navlink1?>" class="nav-link"><?=$navitem1?></a></li>
					<li class="nav-item"><a href="<?=$navlink2?>" class="nav-link"><?=$navitem2?></a></li>
					<li class="nav-item"><a href="<?=$navlink3?>" class="nav-link"><?=$navitem3?></a></li>
				</ul>
			</nav>
			<?php
			//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
			if (strlen($_SESSION['id']==0)) {
				//Wenn Benutzer nicht angemeldet
				session_destroy(); // Löscht alle Sessionvariablen
				echo"<nav class='account'>
						<a class='nav-link' href='/Login' target='_self'>Login</a>
						<aside id='account_placeholder'>/</aside>
						<a class='nav-link' href='/Register' target='_self'>Register</a>
					</nav>";
			} else {
				echo"<nav class='account'>
						<a id='accountpopup'>" . $_SESSION['login'] . "&#11206;</a>
						<a id='accountpopup' href='/logout.php' target='_self'>Logout</a>
					</nav>";
			}?>
		</div>
		<div id="main">
			<h2>Costs calculation</h2>
			<!---->
			<hr>
			<!---->
			<div id="calculation">
				<section id="box1">
					<label>Scooter model</label>
					<!-- Hinzufügen der Scooter namen aus der Datenbank / ins Dropdown-->
					<select id="models">
						<?php
							for ($i=1; $i<=count($scooter); $i++) {
								?><option value="<?=$scooter[$i]['serialno']?>"><?=$scooter[$i]['model']?></option><?php 
							}
						?>
					</select>
					<label>Route traveled: (in minutes)</label>
					<input type="text" id="traveledroute" placeholder="Minutes" onchange="main();">
				</section>
				<section id="box2">
					<label>Km/h</label>
					<input type="text" id="kmh" placeholder="Output" readonly>
					<label>Route price:</label>
					<input type="text" id="tripprice" placeholder="Output" readonly>
					<label>Charge runtime:</label>
					<input type="text" id="runtime" placeholder="Output" readonly>
					<label>Power:</label>
					<input type="text" id="power" placeholder="Output" readonly>
					<label>Full recharge costs:</label>
					<input type="text" id="chargecosts" placeholder="Output" readonly>
					<p id="errormsg"></p>
				</section>
			</div>
		</div>
		<div id="footer">
		  <p>
			<span><?php echo $servername; ?> © <?php echo date("Y"); ?></span>
			<a class="social" href="<?php echo $privacypolicylink; ?>" target="_blank" ><?php echo $privacypolicy;?></a>
			<a class="social" href="<?php echo $moodlelink; ?>" target="_blank" ><?php echo $moodle;?></a>
		  </p>
		</div>
	</body>
</html>
