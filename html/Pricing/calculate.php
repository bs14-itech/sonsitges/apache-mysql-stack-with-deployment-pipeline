<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');
include($_SERVER["DOCUMENT_ROOT"].'/Settings/costcalculation.php'); //Importieren der Kostenfunktion

//Erstellung eines Objekts
$json = new stdClass; //Wir erstellen eine neue Klasse
//Wir überprüfen ob beide User inputs gesetzt sind
if(isset($_POST['serialno']) AND isset($_POST['traveledroute'])) {
	//Wenn erfolgreich, dann setzen wir unsere Variablen gleich mit den Eingaben des Users
	$traveledroute = $_POST['traveledroute'];
	$serialno = $_POST['serialno'];
	//Übergabe der Variablen an Javascript & Hinzufügen neuer Werte zur Klasse
	$json->traveledroute = $traveledroute; 
	$json->serialno = $serialno;			
	
	include($_SERVER["DOCUMENT_ROOT"].'/Settings/conn.php');//Datenbankverbindung
	//SQL Abfragen
	$q_getserialno = "SELECT * FROM Models Where serialno=".$serialno;

	//Erstellen des Arrays von den results der Datenbank
	$sql = mysqli_query($con, $q_getserialno);
	$data=mysqli_fetch_array($sql);

	mysqli_close($con);//Datenbankverbindung schließen
	//Database Daten der Klasse hinzufügen
	$json->kmh = $data['kmh'];
	$json->power = $data['watt'];
	$json->currency = $currency;
	
	$result = calculate($data['kmh'], $data['watt'], $data['amper'], $data['voltage'], $traveledroute);
	//Berechnete Daten der Klasse hinzufügen
	$json->runtime = $result[0];
	$json->chargecosts = $result[1];
	$json->tripprice = $result[2];
	echo json_encode($json);
}
?>