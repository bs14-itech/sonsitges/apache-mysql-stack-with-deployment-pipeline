<?php
	//Main
	$fav = "/img/fav.ico";
	$serverlogo = "ScooTeq";
	$servername = "ScooTeq | GmbH";
	$currency = "€";
	$energycosts = 0.32; //Energy costs per kw/h
	$minprice = 1; // Min price
	$defaulttime = 15; //15 Minuten
	
	//Navigation bar
	$navitem1 = "Pricing";
	$navlink1 = "/Pricing";
	$navitem2 = "Models";
	$navlink2 = "/Models";
	$navitem3 = "Contact";
	$navlink3 = "/Contact";
	
	//Footer
	$privacypolicy = "Privacy Policy";
	$privacypolicylink = "../Privacy";
	$moodle = "ITECH Moodle";
	$moodlelink = "https://moodle.itech-bs14.de/";
	$twitter = "<a href='https://twitter.com/DarkvoidNetwork' target='_blank' class='social'>TWITTER</a>";
	$steamgroup = "<a href='https://steamcommunity.com/groups/DarkvoidNetwork' target='_blank' class='social'>STEAM GROUP</a>";
?>
