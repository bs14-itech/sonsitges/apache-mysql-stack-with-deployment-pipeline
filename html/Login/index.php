<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/conn.php');//Datenbankverbindung
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');//Main Settings

//Prüfen ob der Benutzer angemeldet ist
if (strlen($_SESSION['id']!=0)) {
		//Wenn erfolgreich dann, weiterleitung an Main-Page
		header('location:/');
	}

//Hauptcode zum Anmelden
//Prüfen ob der Knopf gedrückt wurde
if(isset($_POST['login'])) {
	//Speichern der Benutzer eingaben in Variablen
	$username = $_POST['username'];
	$password = $_POST['password'];
	//Passwort in MD5 verschlüsseln
	$hashed_password = md5($password);
	//Wir prüfen ob es in der Tabelle "User" einen Benutzernamen und ein Passwort gibt so wie es der Benutzer eingeben hat
	$sql = mysqli_query($con,"SELECT * FROM User WHERE username='$username' and password='$hashed_password'");
	//Erstellen des Arrays von den results der Datenbank
	$data = mysqli_fetch_array($sql);
	// Prüft ob das result der Datenbank nicht 0 ist 
	if($data>0) {
		//Wenn erfolgreich dann, speichern wir die Sessionvariablen
		$_SESSION['login']=$username;
		$_SESSION['id']=$data['id'];
		//Setzt in der Datenbank den letzten Anmeldeversuch des Benutzers auf JETZT
		$lastlogin = mysqli_query($con,"UPDATE User SET last_login=NOW() WHERE id=('$_SESSION[id]')");
		//Weiterleitung an Main-Page
		header('location:/');
		exit(); //Beendet den PHP-Code
	} else {
		//Wenn nicht erfolgreich dann, bekommt die Sessionvariable "err" eine Fehlernachricht
		$_SESSION['err']="Invalid username or password";
		header("location:index.php"); //Weiterleitung an die interne Datei "index.php" (Refresh)
		exit(); //Beendet den PHP-Code
	}
}
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $servername;?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $fav;?>">
	<!-- Schriftarten -->
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
	<!-- CSS Datein -->
	<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
</head>
<body>
	<div class="header">
		<!-- Navigation -->
		<nav class="navigation">
			<a class="logo" href="/" target="_self"><?=$serverlogo?></a>
			<ul class="nav">
				<li class="nav-item"><a href="<?=$navlink1?>" class="nav-link"><?=$navitem1?></a></li>
				<li class="nav-item"><a href="<?=$navlink2?>" class="nav-link"><?=$navitem2?></a></li>
				<li class="nav-item"><a href="<?=$navlink3?>" class="nav-link"><?=$navitem3?></a></li>
			</ul>
		</nav>
		<?php
		//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
		if (strlen($_SESSION['id']==0)) {
			//Wenn Benutzer nicht angemeldet
			session_destroy(); // Löscht alle Sessionvariablen
			echo"<nav class='account'>
					<a class='nav-link-active' href='/Login' target='_self'>Login</a>
					<aside id='account_placeholder' style='color:white;'>/</aside>
					<a class='nav-link' href='/Register' target='_self'>Register</a>
				</nav>";
		} else {
			echo"<nav class='account'>
					<a class='nav-link' href='/Login' target='_self'>" . $_SESSION['login'] . "</a>
				</nav>";
		}?>
		</div>
		<form method="post" action="" id="form"> <!-- Erstellen eines Webfomulars mit der Methode "POST" -->
			<h1>Login</h1>
			<!-- Erstellen der Eingabefelder -->
			<input type="text" name="username" placeholder="Username">
			<input type="password" name="password" placeholder="Password">
			<input type="submit" name="login" value="Submit">
			<?php
			if (isset($_SESSION['err'])) { //Prüft ob die Sessionvariable "err" gesetzt
				//Wenn ja, dann erstellen wir ein HTML-Element (p) und geben den Wert Sessionvariable aus.
				echo "<p id='errormsg'>" . $_SESSION['err'] . "</p>";
				$_SESSION['err']=NULL; //Setzt die Sessionvariable auf den Wert NULL(nichts)
			}
			?>
		</form>
	<div id="footer">
	  <p>
		<span><?=$servername?> © <?=date("Y")?></span>
		<a class="social" href="<?=$privacypolicylink?>" target="_blank" ><?=$privacypolicy?></a>
		<a class="social" href="<?=$moodlelink?>" target="_blank" ><?=$moodle?></a>
	  </p>
	</div>
	</body>
</html>
