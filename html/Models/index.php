<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');//Main Settings
include($_SERVER["DOCUMENT_ROOT"].'/Settings/getScooter.php'); //Importieren der Scooter aus der Datenbank

?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<!-- Zeichen Koodierung -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$servername?></title>
		<!-- Website Title Logo -->
		<link rel="shortcut icon" type="image/x-icon" href="<?=$fav?>">
		<!-- Schriftarten -->
		<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
		<!-- CSS Datein -->
		<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
	</head>
	<body>
		<div class="header">
			<nav class="navigation">
				<a class="logo" href="/" target="_self"><?=$serverlogo?></a>
				<ul class="nav">
					<li class="nav-item"><a href="<?=$navlink1?>" class="nav-link"><?=$navitem1?></a></li>
					<li class="nav-item"><a href="<?=$navlink2?>" class="nav-link"><?=$navitem2?></a></li>
					<li class="nav-item"><a href="<?=$navlink3?>" class="nav-link"><?=$navitem3?></a></li>
				</ul>
			</nav>
			<?php
			//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
			if (strlen($_SESSION['id']==0)) {
				//Wenn Benutzer nicht angemeldet
				session_destroy(); // Löscht alle Sessionvariablen
				echo"<nav class='account'>
						<a class='nav-link' href='/Login' target='_self'>Login</a>
						<aside id='account_placeholder'>/</aside>
						<a class='nav-link' href='/Register' target='_self'>Register</a>
					</nav>";
			} else {
				echo"<nav class='account'>
						<a id='accountpopup'>" . $_SESSION['login'] . "&#11206;</a>
						<a id='accountpopup' href='/logout.php' target='_self'>Logout</a>
					</nav>";
			}?>
		</div>
		<div id="main">
			<h2>Our Scooter models</h2>
			<aside id="updated"><?="Calculated with ".$defaulttime." minutes"?></aside>
			<!---->
			<hr>
			<!---->
			<section id="server">
				<?php
					include($_SERVER["DOCUMENT_ROOT"].'/Settings/costcalculation.php'); //Kosten berechnungsfunktion
					//Berechnen der Scooter Runtime, Full recharge costs & Runtime
					for ($i=1; $i<=count($scooter); $i++) {
						$result = calculate($scooter[$i]['kmh'], $scooter[$i]['watt'], $scooter[$i]['amper'], $scooter[$i]['voltage'], $defaulttime);
						?>
						<!-- Scooter Models -->
						<div class="boxborder">
							<h1><?=$scooter[$i]['model']?></h1>
							<img class="imgview" src="img/Scooter<?=$i?>.jpg">
							<h3><?=$scooter[$i]['type']?> vehicle</h3>
							<h4 class="headline">Speed</h4>
							<aside class="technicaldata"><?=$scooter[$i]['kmh']?> km/h</aside>
							<h4 class="headline">Driving costs</h4>
							<aside class="technicaldata"><?=$result[2].$currency?></aside>
							<h4 class="headline">Charge runtime</h4>
							<aside class="technicaldata"><?=$result[0]."min"?></aside>
							<h4 class="headline">Power</h4>
							<aside class="technicaldata"><?=$scooter[$i]['watt']."W"?></aside>
							<h4 class="headline">Full recharge costs</h4>
							<aside class="technicaldata"><?=$result[1].$currency?></aside>
						</div>
				<?php 
					}
				?>
			</section>
		</div>
		<div id="footer">
		  <p>
			<span><?=$servername?> © <?=date("Y")?></span>
			<a class="social" href="<?=$privacypolicylink?>" target="_blank" ><?=$privacypolicy?></a>
			<a class="social" href="<?=$moodlelink?>" target="_blank" ><?=$moodle?></a>
		  </p>
		</div>
	</body>
</html>