<?php
session_start(); //Importieren aller Sessionvariablen (Session Cookies)
include($_SERVER["DOCUMENT_ROOT"].'/Settings/conn.php');//Datenbankverbindung
include($_SERVER["DOCUMENT_ROOT"].'/Settings/mainsettings.php');//Main Settings

//Hauptcode zum Kontaktieren
if(isset($_POST['submit'])) {
	//Speichern der Benutzer eingaben in Variablen
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$email = $_POST['email'];
	$message = $_POST['text'];
	
	//Setup Email
	$empfaenger = 'Support@ScooTeq.com';
	$betreff = 'Website question';
	$nachricht = $message;
	$header = 'From: '.$email;
	
	//Email Abschicken
	mail($empfaenger, $betreff, $nachricht, $header);
}
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<!-- Zeichen Koodierung -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$servername?></title>
		<!-- Website Title Logo -->
		<link rel="shortcut icon" type="image/x-icon" href="<?=$fav?>">
		<!-- Schriftarten -->
		<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
		<!-- CSS Datein -->
		<link rel="stylesheet" id="parent-style-css" href="css/style.css" type="text/css" media="all" />
	</head>
	<body>
		<div class="header">
			<!-- Navigation -->
			<nav class="navigation">
				<a class="logo" href="/" target="_self"><?=$serverlogo?></a>
				<ul class="nav">
					<li class="nav-item"><a href="<?=$navlink1?>" class="nav-link"><?=$navitem1?></a></li>
					<li class="nav-item"><a href="<?=$navlink2?>" class="nav-link"><?=$navitem2?></a></li>
					<li class="nav-item"><a href="<?=$navlink3?>" class="nav-link"><?=$navitem3?></a></li>
				</ul>
			</nav>
			<?php
			//Wir prüfen ob die länge der Sessionvariable 0 ist (ob er angemeldet ist)
			if (strlen($_SESSION['id']==0)) {
				//Wenn Benutzer nicht angemeldet
				session_destroy(); // Löscht alle Sessionvariablen
				echo"<nav class='account'>
						<a class='nav-link' href='/Login' target='_self'>Login</a>
						<aside id='account_placeholder'>/</aside>
						<a class='nav-link' href='/Register' target='_self'>Register</a>
					</nav>";
			} else {
				echo"<nav class='account'>
						<a id='accountpopup'>" . $_SESSION['login'] . "&#11206;</a>
						<a id='accountpopup' href='/logout.php' target='_self'>Logout</a>
					</nav>";
			}?>
		</div>
		<div id="main">
			<h2>Contact</h2>
			<!---->
			<hr>
			<!---->
			<div id="box2">
				<div id="box3">
					<form method="post" action="" id="form"> <!-- Erstellen eines Webfomulars mit der Methode "POST" -->
						<h1>Send us an Email</h1>
						<!-- Erstellen der Eingabefelder -->
						<input type="text" name="firstname" placeholder="Firstname">
						<input type="text" name="lastname" placeholder="Lastname">
						<input type="text" name="email" placeholder="E-Mail"><br/>
						<textarea name="text" placeholder="Message to support"></textarea>
						<input type="submit" name="submit" value="Submit">		
					</form>
				</div>
				<div id="box4">
					<iframe id="maps" width="520" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Unagi%20Scooters%201405%20Abbot%20Kinney%20Blvd%20Los%20Angeles+(Los%20Angeles)&amp;t=&amp;z=11&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> <a href='https://www.acadoo-medizin.com/'>acadoo-medizin.com</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=0a80c98decb034383219e4f5f782580dfafad64e'></script>
				</div>
			</div>
		</div>
		<div id="footer">
		  <p>
			<span><?=$servername?> © <?=date("Y")?></span>
			<a class="social" href="<?=$privacypolicylink?>" target="_blank" ><?=$privacypolicy?></a>
			<a class="social" href="<?=$moodlelink?>" target="_blank" ><?=$moodle?></a>
		  </p>
		</div>
	</body>
</html>
