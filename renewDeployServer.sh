if [-d ~/git]
then mkdir ~/git
fi;
cd ~/git
git pull git@gitlab.com:fkplayground/database/database-webserver/fabiskrimskrams.git
sudo docker stop logindb loginweb
sudo docker rmi loginweb
sudo docker rm logindb loginweb
cd $CI_PROJECT_NAME
sudo docker-compose up -d
sudo sh renewDatabase.sh
